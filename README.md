# CNHorses

## About
CNHorses allows you to store your horses as an item, to transport and protects them from thieves. 

## Usage
Simply take the saddle (or carpet from an Llama) from the horse to store it, or right-click with the saddle to release it.

## Features
* The horse is stored in the horse's NBT data. (This means, no database, or faulty lore parsing.)
* Protection against thieves. (Only you can store your horse.)
* World restriction. (You can define in which world the horses can be placed/stored.)
* Supports all kinds of horses & llamas.
* Doesn't require an update when Spigot updates (Unlike some other similar plugins). Compatible with 1.11+
