package de.cubenation.cnhorses.command;

import de.cubenation.bedrock.BasePlugin;
import de.cubenation.bedrock.command.CommandRole;
import de.cubenation.bedrock.command.PlayerCommand;
import de.cubenation.bedrock.command.argument.Argument;
import de.cubenation.bedrock.command.manager.CommandManager;
import de.cubenation.bedrock.exception.CommandException;
import de.cubenation.bedrock.exception.IllegalCommandArgumentException;
import de.cubenation.bedrock.exception.InsufficientPermissionException;
import de.cubenation.bedrock.permission.Permission;
import de.cubenation.bedrock.service.confirm.ConfirmInterface;
import de.cubenation.bedrock.service.confirm.ConfirmRegistry;
import de.cubenation.cnhorses.messages.Messages;
import org.bukkit.entity.Player;

import java.util.ArrayList;

/**
 * Created by bhruschka on 30.01.17.
 * Project: CNHorses
 */
public class ConfirmCommand extends PlayerCommand {

    public ConfirmCommand(BasePlugin plugin, CommandManager commandManager) {
        super(plugin, commandManager);
    }

    @Override
    public void setPermissions(ArrayList<Permission> permissions) {
        permissions.add(new Permission("confirm", CommandRole.USER));
    }

    @Override
    public void setSubCommands(ArrayList<String[]> subCommands) {
        subCommands.add(new String[]{"confirm"});
    }

    @Override
    public void setDescription(StringBuilder description) {
        description.append("command.confirm.desc");
    }

    @Override
    public void setArguments(ArrayList<Argument> arguments) {
    }

    @Override
    public void execute(Player player, String[] args) throws CommandException, IllegalCommandArgumentException, InsufficientPermissionException {

        ConfirmInterface ci = ConfirmRegistry.getInstance().get(player);

        if (ci == null) {
            Messages.Confirm.Nothing(player);
            return;
        }

        // remove sender from ConfirmService Registry
        ConfirmRegistry.getInstance().remove(player);

        ci.call();
    }
}
