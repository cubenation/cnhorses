package de.cubenation.cnhorses.command;

import de.cubenation.bedrock.BasePlugin;
import de.cubenation.bedrock.callback.MultipleBedrockPlayerCallback;
import de.cubenation.bedrock.callback.SingleBedrockPlayerCallback;
import de.cubenation.bedrock.command.CommandRole;
import de.cubenation.bedrock.command.PlayerCommand;
import de.cubenation.bedrock.command.argument.Argument;
import de.cubenation.bedrock.command.manager.CommandManager;
import de.cubenation.bedrock.ebean.BedrockPlayer;
import de.cubenation.bedrock.exception.CommandException;
import de.cubenation.bedrock.exception.IllegalCommandArgumentException;
import de.cubenation.bedrock.exception.InsufficientPermissionException;
import de.cubenation.bedrock.helper.BedrockEbeanHelper;
import de.cubenation.bedrock.helper.UUIDUtil;
import de.cubenation.bedrock.permission.Permission;
import de.cubenation.bedrock.service.confirm.ConfirmRegistry;
import de.cubenation.bedrock.service.confirm.ConfirmStorable;
import de.cubenation.cnhorses.CNHorses;
import de.cubenation.cnhorses.item.HorseSaddle;
import de.cubenation.cnhorses.messages.Messages;
import de.cubenation.cnhorses.util.ConfirmHorseTransfer;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by bhruschka on 30.01.17.
 * Project: CNHorses
 */
public class TransferCommand extends PlayerCommand {

    private Permission transferOtherPermission;

    public TransferCommand(BasePlugin plugin, CommandManager commandManager) {
        super(plugin, commandManager);
    }

    @Override
    public void setPermissions(ArrayList<Permission> permissions) {

        transferOtherPermission = new Permission("transfer.horse.other", CommandRole.MODERATOR);

        permissions.add(new Permission("transfer.horse", CommandRole.USER));
        permissions.add(transferOtherPermission);
    }

    @Override
    public void setSubCommands(ArrayList<String[]> subCommands) {
        subCommands.add(new String[]{"transfer"});
    }

    @Override
    public void setDescription(StringBuilder description) {
        description.append("command.transfer.horse.desc");
    }

    @Override
    public void setArguments(ArrayList<Argument> arguments) {
        arguments.add(new Argument("command.arg.name.desc", "command.arg.name.ph"));
    }

    @Override
    public void execute(final Player player, final String[] args) throws CommandException, IllegalCommandArgumentException, InsufficientPermissionException {

        final ItemStack itemInMainHand = player.getInventory().getItemInMainHand();
        if (itemInMainHand == null || itemInMainHand.getType() == Material.AIR) {
            Messages.Error.EmptyHand(player);
            return;
        }

        HorseSaddle horseSaddle = new HorseSaddle(itemInMainHand);
        if (!horseSaddle.isHorseSaddle()) {
            Messages.Error.InvalidSaddle(player);
            return;
        }

        if (!horseSaddle.getOwnerUuid().equals(player.getUniqueId())) {
            if (!transferOtherPermission.userHasPermission(player)) {
                Messages.StrangerHorse(player, horseSaddle.getVariant());
                return;
            } else {
                Messages.Bypass.Info(player);
            }
        }

        if (!UUIDUtil.isUUID(args[0])) {
            BedrockEbeanHelper.requestBedrockPlayerForLastKnownName(args[0], true, new MultipleBedrockPlayerCallback() {
                @Override
                public void didFinished(List<BedrockPlayer> list) {
                    if (list == null || list.isEmpty()) {
                        Messages.Error.PlayerNotFound(player, args[0]);
                        return;
                    }
                    BedrockPlayer bedrockPlayer = list.get(0);
                    transfer(player, horseSaddle, bedrockPlayer);
                }

                @Override
                public void didFailed(Exception e) {
                    Messages.Error.PlayerNotFound(player, args[0]);
                }
            });
        } else {
            BedrockEbeanHelper.requestBedrockPlayer(UUID.fromString(args[0]), new SingleBedrockPlayerCallback() {
                @Override
                public void didFinished(BedrockPlayer bedrockPlayer) {
                    transfer(player, horseSaddle, bedrockPlayer);
                }

                @Override
                public void didFailed(Exception e) {
                    Messages.Error.PlayerForUUIDNotFound(player, args[0]);
                }
            });
        }
    }

    private void transfer(Player player, HorseSaddle horseSaddle, BedrockPlayer bedrockPlayer) {

        ConfirmHorseTransfer confirm = new ConfirmHorseTransfer(CNHorses.getInstance());
        confirm.store("sender", new ConfirmStorable<Object>(player));
        confirm.store("receiverUuid", new ConfirmStorable<Object>(bedrockPlayer.getUUID()));
        confirm.store("receiverName", new ConfirmStorable<Object>(bedrockPlayer.getUsername()));
        confirm.store("horseUuid", new ConfirmStorable<Object>(horseSaddle.getHorseUuid()));
        ConfirmRegistry.getInstance().put(player, confirm);

        Messages.Confirm.Transfer(player);
    }

}

