package de.cubenation.cnhorses.command;

import de.cubenation.bedrock.BasePlugin;
import de.cubenation.bedrock.command.Command;
import de.cubenation.bedrock.command.CommandRole;
import de.cubenation.bedrock.command.argument.Argument;
import de.cubenation.bedrock.command.manager.CommandManager;
import de.cubenation.bedrock.exception.CommandException;
import de.cubenation.bedrock.exception.ConfigNotContainsObjectException;
import de.cubenation.bedrock.exception.IllegalCommandArgumentException;
import de.cubenation.bedrock.exception.InsufficientPermissionException;
import de.cubenation.bedrock.permission.Permission;
import de.cubenation.cnhorses.config.WorldConfig;
import de.cubenation.cnhorses.manager.WorldManager;
import de.cubenation.cnhorses.messages.Messages;
import org.bukkit.World;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;

/**
 * Created by bhruschka on 29.01.17.
 * Project: CNHorses
 */
public class RemoveWorldCommand extends Command {


    public RemoveWorldCommand(BasePlugin plugin, CommandManager commandManager) {
        super(plugin, commandManager);
    }

    @Override
    public void setPermissions(ArrayList<Permission> permissions) {
        permissions.add(new Permission("worlds.remove", CommandRole.ADMIN.getType()));
    }

    @Override
    public void setSubCommands(ArrayList<String[]> subCommands) {
        subCommands.add(new String[] { "world" , "worlds" } );
        subCommands.add(new String[] { "r", "rm", "remove", "-" } );
    }

    @Override
    public void setDescription(StringBuilder description) {
        description.append("command.worlds.remove.desc");
    }

    @Override
    public void setArguments(ArrayList<Argument> arguments) {
        arguments.add(new Argument("command.worlds.add.args.world.desc", "command.worlds.add.args.world.ph", true));
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) throws CommandException, IllegalCommandArgumentException, InsufficientPermissionException {
        World world = WorldManager.getWorld(commandSender, args);

        if (world == null) {
            Messages.noSuchWorld(plugin, commandSender, (args.length > 0 ? args[0] : ""));
            return;
        }

        WorldConfig worldConfig = (WorldConfig) plugin.getConfigService().getConfig(WorldConfig.class);

        try {
            if (worldConfig.removeWorld(world)) {
                Messages.Worlds.removeSuccessful(commandSender, world);
            } else {
                Messages.Worlds.error(commandSender, world);
            }
        } catch (ConfigNotContainsObjectException e) {
            Messages.Worlds.configContainsNot(commandSender, world);
        }

    }

    @Override
    public ArrayList<String> getTabArgumentCompletion(CommandSender sender, int argumentIndex, String[] args) {
        if (argumentIndex == 0) {
            return WorldManager.getInstance().getEnabledWorldNames();
        }
        return null;
    }
}
