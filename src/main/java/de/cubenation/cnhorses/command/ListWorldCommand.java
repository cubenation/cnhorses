package de.cubenation.cnhorses.command;

import de.cubenation.bedrock.BasePlugin;
import de.cubenation.bedrock.command.Command;
import de.cubenation.bedrock.command.CommandRole;
import de.cubenation.bedrock.command.argument.Argument;
import de.cubenation.bedrock.command.manager.CommandManager;
import de.cubenation.bedrock.exception.CommandException;
import de.cubenation.bedrock.exception.IllegalCommandArgumentException;
import de.cubenation.bedrock.exception.InsufficientPermissionException;
import de.cubenation.bedrock.permission.Permission;
import de.cubenation.cnhorses.config.WorldConfig;
import de.cubenation.cnhorses.messages.Messages;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;

/**
 * Created by bhruschka on 29.01.17.
 * Project: CNHorses
 */
public class ListWorldCommand extends Command {

    public ListWorldCommand(BasePlugin plugin, CommandManager commandManager) {
        super(plugin, commandManager);
    }

    @Override
    public void setPermissions(ArrayList<Permission> permissions) {
        permissions.add(new Permission("worlds.list", CommandRole.ADMIN.getType()));
    }

    @Override
    public void setSubCommands(ArrayList<String[]> subCommands) {
        subCommands.add(new String[] { "world" , "worlds" } );
        subCommands.add(new String[] { "list", "l" } );
    }

    @Override
    public void setDescription(StringBuilder description) {
        description.append("command.worlds.list.desc");
    }

    @Override
    public void setArguments(ArrayList<Argument> arguments) {
    }

    @Override
    public void execute(CommandSender commandSender, String[] commands) throws CommandException, IllegalCommandArgumentException, InsufficientPermissionException {

        WorldConfig worldConfig = (WorldConfig) plugin.getConfigService().getConfig(WorldConfig.class);

        if (worldConfig.getWorlds().isEmpty()) {
            Messages.Worlds.listEmpty(commandSender);
            return;
        }

        Messages.Worlds.list(commandSender, worldConfig.getWorlds());
    }
}

