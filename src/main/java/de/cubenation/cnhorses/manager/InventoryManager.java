package de.cubenation.cnhorses.manager;

import de.cubenation.cnhorses.item.HorseSaddle;
import org.bukkit.block.ShulkerBox;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.HorseInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.LlamaInventory;

/**
 * Created by bhruschka on 05.02.17.
 * Project: CNHorses
 */
public class InventoryManager {

    private static InventoryManager instance = new InventoryManager();

    public static InventoryManager getInstance() {
        return instance;
    }

    private InventoryManager() {
    }

    public boolean preventHorseInPortableInventory(InventoryClickEvent event) {
        if (!isPortableInventory(event.getInventory())) {
            return false;
        }

        ItemStack currentItem = event.getCurrentItem();
        if (currentItem == null) {
            return false;
        }

        HorseSaddle horseSaddle = new HorseSaddle(currentItem);
        return horseSaddle.isHorseSaddle();
    }

    public boolean preventShulkerInPortableInventory(InventoryClickEvent event) {
        if (!isPortableInventory(event.getInventory())) {
            return false;
        }

        ItemStack currentItem = event.getCurrentItem();

        // Ugly, but i'm not copy-pasting 16 times currentItem.getType() == Material.WHITE_SHULKER_BOX .....
        return currentItem.getType().getId() >= 219 && currentItem.getType().getId() <= 234;
    }


    @SuppressWarnings("RedundantIfStatement")
    private boolean isPortableInventory(Inventory inventory) {
        if (inventory instanceof HorseInventory || inventory instanceof LlamaInventory) {
            return true;
        }

        if (inventory.getHolder() instanceof ShulkerBox) {
            return true;
        }

        return false;
    }
}
