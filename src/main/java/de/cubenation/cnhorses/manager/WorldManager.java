package de.cubenation.cnhorses.manager;

import de.cubenation.bedrock.helper.UUIDUtil;
import de.cubenation.bedrock.reloadable.Reloadable;
import de.cubenation.cnhorses.CNHorses;
import de.cubenation.cnhorses.config.WorldConfig;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by bhruschka on 29.01.17.
 * Project: CNHorses
 */
public class WorldManager implements Reloadable {

    CNHorses plugin = CNHorses.getInstance();

    private static WorldManager instance;

    private ArrayList<String> enabledWorldNames = new ArrayList<>();
    private ArrayList<String> disabledWorldNames = new ArrayList<>();

    public static WorldManager getInstance() {
        if (instance == null) {
            instance = new WorldManager();
        }
        return instance;
    }

    private WorldManager() {
        reload();
    }

    private ArrayList<String> getWorldNames(ArrayList<World> worlds) {
        ArrayList<String> worldNames = new ArrayList<>();

        for (World world : worlds) {
            worldNames.add(world.getName());
        }

        return worldNames;
    }

    @Override
    public void reload() {
        enabledWorldNames.clear();
        disabledWorldNames.clear();

        WorldConfig config = (WorldConfig) plugin.getConfigService().getConfig(WorldConfig.class);

        enabledWorldNames = getWorldNames(config.getWorlds());

        ArrayList<World> inactiveWorlds = (ArrayList<World>) Bukkit.getServer().getWorlds();
        inactiveWorlds.removeAll(config.getWorlds());
        disabledWorldNames = getWorldNames(inactiveWorlds);
    }


    public ArrayList<String> getEnabledWorldNames() {
        return enabledWorldNames;
    }

    public ArrayList<String> getDisabledWorldNames() {
        return disabledWorldNames;
    }


    public static World getWorld(CommandSender commandSender, String[] args) {
        World world = null;
        if (args.length > 0) {
            if (UUIDUtil.isUUID(args[0])) {
                world = Bukkit.getServer().getWorld(UUID.fromString(args[0]));
            } else {
                world = Bukkit.getServer().getWorld(args[0]);
            }
        } else {
            if (commandSender instanceof Player) {
                world = ((Player) commandSender).getWorld();
            }
        }
        return world;
    }
}

