package de.cubenation.cnhorses.manager;

import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.managers.RegionManager;
import de.cubenation.cnhorses.CNHorses;
import de.cubenation.cnhorses.config.HorseConfig;
import de.cubenation.cnhorses.config.WorldConfig;
import de.cubenation.cnhorses.item.HorseSaddle;
import de.cubenation.cnhorses.messages.Messages;
import de.cubenation.cnhorses.util.HorseSaddleLogEntry;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.AbstractHorse;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import java.util.Collection;
import java.util.UUID;

/**
 * Created by bhruschka on 28.01.17.
 * Project: CNHorses
 */
public class HorseManager {

    private static HorseManager instance = new HorseManager();

    public static HorseManager getInstance() {
        return instance;
    }

    private HorseManager() {
    }

    private final CNHorses plugin = CNHorses.getInstance();

    public static String horse_store = "horse.store";
    public static String horse_store_other = "horse.store.other";
    public static String horse_place = "horse.place";
    public static String horse_place_other = "horse.place.other";

    public static String bypass_horse_store = "bypass.horse.store";
    public static String bypass_horse_place = "bypass.horse.store";

    public boolean handleHorseStore(InventoryClickEvent event) {
        HorseConfig horseConfig = (HorseConfig) plugin.getConfigService().getConfig(HorseConfig.class);
        if (!horseConfig.isStoreEnabled()) {
            return false;
        }

        Player player = (Player) event.getWhoClicked();

        if (!plugin.getPermissionService().hasPermission(player, horse_store)) {
            return false;
        }

        // Null checks
        if (event.getClickedInventory() == null) {
            return false;
        }

        // Check that the inventory belongs to a horse
        if (!(event.getClickedInventory().getHolder() instanceof AbstractHorse)) {
            return false;
        }

        if (event.getSlot() != 0 && event.getSlot() != 1) {
            return false;
        }

        if (event.getCurrentItem().getType() != Material.SADDLE && event.getCurrentItem().getType() != Material.CARPET) {
            return false;
        }

        // Don't continue if we're ignoring shift-clicks
        if (horseConfig.isStoreShiftClickIgnored()) {
            if (event.getAction() != InventoryAction.PICKUP_ALL) {
                return false;
            }
        }

        if (!isPlaceStoreAllowed(player, CNHorses.HORSES_STORE)) {
            if (!plugin.getPermissionService().hasPermission(player, bypass_horse_store)) {
                // No Message needed.
                return false;
            } else {
                Messages.Bypass.Info(player);
            }
        }

        AbstractHorse abstractHorse = (AbstractHorse) event.getClickedInventory().getHolder();

        if (player.getInventory().firstEmpty() == -1) {
            Messages.InventoryIsFull(player, abstractHorse);
            return true;
        }


        if (abstractHorse.getOwner() == null) {
            abstractHorse.setOwner(player);
            plugin.logBrokenOwner(player, "Broken Owner");
        }

        if (abstractHorse.getOwner().getUniqueId() == null) {
            abstractHorse.setOwner(player);
            plugin.logBrokenOwner(player, "Broken Owner UUID");
        }

        if (abstractHorse.getOwner().getName() == null) {
            abstractHorse.setOwner(player);
            plugin.logBrokenOwner(player, "Broken Owner Name");
        }

        if (!abstractHorse.getOwner().getUniqueId().equals(player.getUniqueId())) {
            if (!plugin.getPermissionService().hasPermission(player, horse_store_other)) {
                Messages.StrangerHorse(player, abstractHorse);
                return true;
            } else {
                Messages.Bypass.Info(player);
            }
        }

        Collection<PotionEffect> activePotionEffects = abstractHorse.getActivePotionEffects();
        for (PotionEffect activePotionEffect : activePotionEffects) {
            abstractHorse.removePotionEffect(activePotionEffect.getType());
        }

        ItemStack saddle = new ItemStack(event.getCurrentItem().getType(), 1);
        HorseSaddle horseSaddle = new HorseSaddle(saddle);
        horseSaddle.fromHorse(abstractHorse);

        saddle = horseSaddle.getItem();

        event.setCurrentItem(new ItemStack(Material.AIR));

        int inventorySize = getInventorySize(abstractHorse);

        // Remove the horse
        abstractHorse.remove();

        player.getInventory().addItem(saddle);

        // Log
        plugin.addHorseSaddleLogs(new HorseSaddleLogEntry(player.getUniqueId(), player.getName(),
                horseSaddle.getHorseName(),
                horseSaddle.getHorseUuid(),
                horseSaddle.getJumpValue(),
                horseSaddle.getSpeedValue(),
                horseSaddle.getHealthValue(),
                horseSaddle.getHorseType(),
                horseSaddle.getVariant(),
                player.getWorld().getName(),
                player.getLocation().getBlockX(),
                player.getLocation().getBlockY(),
                player.getLocation().getBlockZ(),
                "store",
                inventorySize
        ));

        return true;
    }

    public boolean handleHorsePlace(PlayerInteractEvent event) {
        HorseConfig horseConfig = (HorseConfig) plugin.getConfigService().getConfig(HorseConfig.class);
        if (!horseConfig.isPlaceEnabled()) {
            return false;
        }

        Player player = event.getPlayer();

        if (!plugin.getPermissionService().hasPermission(player, horse_place)) {
            return false;
        }

        if (event.getHand() != EquipmentSlot.HAND) {
            return false;
        }
        ItemStack item = player.getInventory().getItemInMainHand();

        // Ensure the player is clicking a block
        if (event.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return false;
        }

        // Null checks!
        if (item == null) {
            return false;
        }

        // Check that it's a saddle or carpet
        if (item.getType() != Material.SADDLE && item.getType() != Material.CARPET) {
            return false;
        }

        // Check that it's a horses saddle
        HorseSaddle horseSaddle = new HorseSaddle(item);
        if (!horseSaddle.isHorseSaddle()) {
            return false;
        }

        if (!isPlaceStoreAllowed(player, CNHorses.HORSES_PLACE)) {
            if (!plugin.getPermissionService().hasPermission(player, bypass_horse_place)) {
                Messages.Error.LocationNotEnabled(player);
                return true;
            } else {
                Messages.Bypass.Info(player);
            }
        }

        Location spawnLocation = new Location(event.getClickedBlock().getWorld(),
                event.getClickedBlock().getX(), event.getClickedBlock().getY() + 1, event.getClickedBlock().getZ());


        UUID ownerUuid = horseSaddle.getOwnerUuid();
        if (ownerUuid != null) {
            if (!ownerUuid.equals(player.getUniqueId())) {
                if (!plugin.getPermissionService().hasPermission(player, horse_place_other)) {
                    Messages.StrangerHorse(player, horseSaddle.getVariant());
                    return true;
                } else {
                    Messages.Bypass.Info(player);
                }
            }
        }
        AbstractHorse abstractHorse = horseSaddle.toHorse(spawnLocation, player);

        // Something went wrong, let's stop
        if (abstractHorse == null) {
            return true;
        }

        player.getInventory().removeItem(item);

        int inventorySize = getInventorySize(abstractHorse);

        // Log
        plugin.addHorseSaddleLogs(new HorseSaddleLogEntry(player.getUniqueId(), player.getName(),
                horseSaddle.getHorseName(),
                horseSaddle.getHorseUuid(),
                horseSaddle.getJumpValue(),
                horseSaddle.getSpeedValue(),
                horseSaddle.getHealthValue(),
                horseSaddle.getHorseType(),
                horseSaddle.getVariant(),
                player.getWorld().getName(),
                player.getLocation().getBlockX(),
                player.getLocation().getBlockY(),
                player.getLocation().getBlockZ(),
                "place",
                inventorySize
        ));
        return true;
    }

    private int getInventorySize(AbstractHorse abstractHorse) {
        int i = 0;
        for (ItemStack itemStack : abstractHorse.getInventory().getContents()) {
            if (itemStack != null) {
                i++;
            }
        }
        return i;
    }

    private boolean isPlaceStoreAllowed(Player player, StateFlag placeStore) {
        WorldConfig config = (WorldConfig) plugin.getConfigService().getConfig(WorldConfig.class);
        boolean worldIsEnabled = config.getWorlds().contains(player.getWorld());

        if (!worldIsEnabled) {
            return false;
        }

        try {
            RegionManager regionManager = WGBukkit.getPlugin().getRegionManager(player.getWorld());
            ApplicableRegionSet applicableRegions = regionManager.getApplicableRegions(player.getLocation());
            LocalPlayer localPlayer = WGBukkit.getPlugin().wrapPlayer(player);

            return applicableRegions.testState(localPlayer, placeStore);
        } catch (Exception ignored) {
            return true;
        }

    }

    public void performTransfer(Player player, UUID receiverUuid, String receiverName, UUID horseUuid) {
        if (horseUuid == null || player == null || receiverUuid == null || receiverName == null) {
            Messages.Error.Default(player);
            return;
        }

        ItemStack itemInMainHand = player.getInventory().getItemInMainHand();
        if (itemInMainHand == null || itemInMainHand.getType() == Material.AIR) {
            Messages.Error.EmptyHand(player);
            return;
        }

        HorseSaddle horseSaddle = new HorseSaddle(itemInMainHand);
        if (!horseSaddle.getHorseUuid().equals(horseUuid)) {
            player.sendMessage(String.valueOf(horseUuid));
            player.sendMessage(String.valueOf(horseSaddle.getHorseUuid()));
            Messages.Error.WrongSaddle(player);
            return;
        }

        String oldOwner = horseSaddle.getOwnerName();

        horseSaddle.updateOwner(receiverUuid, receiverName);

        player.getInventory().setItemInMainHand(horseSaddle.getItem());
        Messages.TransferSuccess(player, oldOwner, receiverName, horseSaddle.getVariant());
    }
}
