package de.cubenation.cnhorses.item;

import de.cubenation.api.itemnbtapi.NBTItem;
import de.cubenation.cnhorses.CNHorses;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.IOException;
import java.util.*;

/**
 * Created by bhruschka on 29.01.17.
 * Project: CNHorses
 */
public class HorseSaddle extends NBTItem {

    private final String CNTYPEKEY = "cn_type";
    private final String CNTYPEVALUE = "cnhorses";

    // STATIC KEYS
    private final String HORSENAME = "cnhorses_horsename";
    private final String HORSEUUID = "cnhorses_horseuuid";
    private final String OWNERNAME = "cnhorses_ownername";
    private final String OWNERUUID = "cnhorses_owneruuid";
    private final String VARIANT = "cnhorses_variant";
    private final String VALUE_JUMP = "cnhorses_value_jump";
    private final String VALUE_REAL_JUMP = "cnhorses_value_jump";
    private final String VALUE_SPEED = "cnhorses_value_speed";
    private final String VALUE_REAL_SPEED = "cnhorses_value_speed";
    private final String VALUE_HEALTH = "cnhorses_value_health";
    private final String DOMESTICATION = "cnhorses_domestication";
    private final String AGE = "cnhorses_age";
    private final String COLOR = "cnhorses_color";
    private final String STYLE = "cnhorses_style";
    private final String CARPETCOLOR = "cnhorses_carpet_color";
    private final String CHESTED = "cnhorses_chested";
    private final String STRENGTH = "cnhorses_strength";
    private final String ENTITYTYPE = "cnhorses_entitytype";

    // STATIC VALUES
    private static final String NONE = "none";

    private enum HorseType {
        HORSE("horse"),
        LLAMA("llama"),
        CHESTEDHORSE("chestedhorse");

        private final String type;

        HorseType(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }

    // Properties

    private short carpetColor = -1;


    public HorseSaddle(ItemStack item) {
        super(item);
    }

    @Override
    public ItemStack getItem() {
        ItemStack item = super.getItem();
        if (carpetColor != -1) {
            item.setDurability(carpetColor);
        }
        return item;
    }

    public void fromHorse(AbstractHorse abstractHorse) {
        setCNType();

        // Invisible Attributes (NBT)
        setString(HORSENAME, abstractHorse.getCustomName() != null ? abstractHorse.getCustomName() : NONE);
        setString(HORSEUUID, abstractHorse.getUniqueId() != null ? abstractHorse.getUniqueId().toString() : NONE);
        setString(OWNERNAME, abstractHorse.getOwner() != null ? abstractHorse.getOwner().getName() : NONE);
        setString(OWNERUUID, abstractHorse.getOwner() != null ? abstractHorse.getOwner().getUniqueId().toString() : NONE);
        setString(VARIANT, abstractHorse.getClass().getSimpleName().replace("Craft", ""));

        // Formula from http://minecraft.gamepedia.com/Horse#Statistics
        double jumpValue = abstractHorse.getAttribute(Attribute.HORSE_JUMP_STRENGTH).getValue();
        double jumpValueInBlocks = -0.1817584952 * Math.pow(jumpValue, 3) + 3.689713992 * Math.pow(jumpValue, 2) + 2.128599134 * jumpValue - 0.343930367;
        jumpValueInBlocks = (double) Math.round(jumpValueInBlocks * 1d) / 1d;

        double speedValue = abstractHorse.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED).getValue();
        double speedValueInBlocks = 43.178 * speedValue - 0.0214;
        speedValueInBlocks = (double) Math.round(speedValueInBlocks * 1d) / 1d;

        setDouble(VALUE_JUMP, jumpValueInBlocks);
        setString(VALUE_REAL_JUMP, String.valueOf(jumpValue));
        setDouble(VALUE_SPEED, speedValueInBlocks);
        setString(VALUE_REAL_SPEED, String.valueOf(speedValue));
        setString(VALUE_HEALTH, String.valueOf(abstractHorse.getHealth()) + "/" + String.valueOf(abstractHorse.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue()));

        setString(DOMESTICATION, String.valueOf(abstractHorse.getDomestication() + "/" + String.valueOf(abstractHorse.getMaxDomestication())));
        setInteger(AGE, abstractHorse.getAge());

        // Store armor
        if (abstractHorse.getUniqueId() != null) {
            String identifier = abstractHorse.getUniqueId().toString();

            ArrayList<ItemStack> inv = new ArrayList<>();
            Collections.addAll(inv, abstractHorse.getInventory().getContents());

            CNHorses.getInstance().getInventoryService().create(identifier, inv.toArray(new ItemStack[inv.size()]), null, -1);
        }

        if (abstractHorse instanceof ChestedHorse) {
            ChestedHorse chestedHorse = (ChestedHorse) abstractHorse;

            setString(ENTITYTYPE, HorseType.CHESTEDHORSE.getType());
            setBoolean(CHESTED, chestedHorse.isCarryingChest());
        }

        if (abstractHorse instanceof Horse) {
            Horse horse = (Horse) abstractHorse;

            setString(ENTITYTYPE, HorseType.HORSE.getType());
            setString(COLOR, horse.getColor().toString());
            setString(STYLE, horse.getStyle().toString());

        } else if (abstractHorse instanceof Llama) {
            Llama llama = (Llama) abstractHorse;

            setString(ENTITYTYPE, HorseType.LLAMA.getType());
            setString(COLOR, llama.getColor().toString());
            carpetColor = llama.getInventory().getDecor().getDurability();
            setString(CARPETCOLOR, getCarpetColorAsString(carpetColor));
            setInteger(STRENGTH, llama.getStrength());
        }


        // Visible Attributes (Name & Lore)

        setLore();
        ItemMeta itemMeta = getItem().getItemMeta();
        itemMeta.setDisplayName("CNHorses Saddle");
        itemMeta = addGlow(itemMeta);
        setItemMeta(itemMeta);

    }

    public AbstractHorse toHorse(Location location, Player player) {

        String horseName = getString(HORSENAME);
        UUID horseUuid = UUID.fromString(getString(HORSEUUID));

        String ownerName = getString(OWNERNAME);
        UUID ownerUuid = UUID.fromString(getString(OWNERUUID));

        String variant = getString(VARIANT);
        String domestication = getString(DOMESTICATION);

        String valuehHealth = getString(VALUE_HEALTH);

        double jump = 0.0D;
        try {
            jump = Double.parseDouble(getString(VALUE_REAL_JUMP));
        } catch (NoSuchElementException e) {
            jump = getDouble(VALUE_JUMP);
        }

        double speed = 0.0D;
        try {
            speed = Double.parseDouble(getString(VALUE_REAL_SPEED));
        } catch (NoSuchElementException e) {
            speed = getDouble(VALUE_SPEED);
        }

        int age = getInteger(AGE);

        String carpetColor = "";
        try {
            carpetColor = getString(CARPETCOLOR);
        } catch (NoSuchElementException e) {
            carpetColor = "WHITE";
        }

        String color = getString(COLOR);
        String style = getString(STYLE);
        int strength = getInteger(STRENGTH);

        // Horse creation

        AbstractHorse abstractHorse;

        try {
            Class<?> clazz = Class.forName("org.bukkit.entity." + translateOldVariants(variant));
            Class<? extends AbstractHorse> subclass = clazz.asSubclass(AbstractHorse.class);
            abstractHorse = location.getWorld().spawn(location, subclass);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }

        if (!horseName.equals(NONE)) {
            abstractHorse.setCustomName(horseName);
        }

        if (!ownerUuid.equals(NONE)) {
            abstractHorse.setOwner(Bukkit.getOfflinePlayer(ownerUuid));
        } else {
            abstractHorse.setOwner(Bukkit.getOfflinePlayer(player.getUniqueId()));
        }

        String[] horseHealth = valuehHealth.replace("Health: ", "").split("/");
        String[] horseDom = domestication.replace("Domestication: ", "").split("/");

        double horseCurrentHealth = Double.valueOf(horseHealth[0]);
        double horseMaxHealth = Double.valueOf(horseHealth[1]);
        int horseCurrentDom = Integer.valueOf(horseDom[0]);
        int horseMaxDom = Integer.valueOf(horseDom[1]);

        abstractHorse.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(horseMaxHealth);
        abstractHorse.setHealth(horseCurrentHealth);
        abstractHorse.setMaxDomestication(horseMaxDom);
        abstractHorse.setDomestication(horseCurrentDom);

        abstractHorse.getAttribute(Attribute.HORSE_JUMP_STRENGTH).setBaseValue(jump);
        abstractHorse.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED).setBaseValue(speed);
        abstractHorse.setAge(age);

        if (abstractHorse instanceof ChestedHorse) {
            ((ChestedHorse) abstractHorse).setCarryingChest(getBoolean(CHESTED));

            if (abstractHorse instanceof Llama) {
                ((Llama) abstractHorse).setStrength(strength);
            }
        }

        // Restore inv
        if (horseUuid != null && !horseUuid.equals("")) {
            String identifier = horseUuid.toString();
            try {
                ItemStack[] itemStacks = CNHorses.getInstance().getInventoryService().get(identifier);
                CNHorses.getInstance().getInventoryService().delete(identifier);

                abstractHorse.getInventory().setContents(itemStacks);
            } catch (IOException e) {
                // No inventory found
            }
        }

        if (abstractHorse instanceof Horse) {
            Horse horse = (Horse) abstractHorse;

            horse.setColor(Horse.Color.valueOf(color));
            horse.setStyle(Horse.Style.valueOf(style));
            horse.getInventory().setSaddle(new ItemStack(Material.SADDLE, 1));

            return horse;
        } else if (abstractHorse instanceof Llama) {
            Llama llama = (Llama) abstractHorse;

            llama.setColor(Llama.Color.valueOf(color));

            ItemStack carpet = new ItemStack(Material.CARPET, 1);
            carpet.setDurability(getCarpetColorAsShort(carpetColor));
            llama.getInventory().setItem(1, carpet);

            return llama;
        } else {
            abstractHorse.getInventory().setItem(0, new ItemStack(Material.SADDLE, 1));
            return abstractHorse;
        }
    }

    public void setCNType() {
        setString(CNTYPEKEY, CNTYPEVALUE);
    }

    public String getCNType() {
        return getString(CNTYPEKEY);
    }

    public boolean isHorseSaddle() {
        String cnType = getCNType();
        return cnType != null && cnType.equals(CNTYPEVALUE);
    }

    public UUID getOwnerUuid() {
        return UUID.fromString(getString(OWNERUUID));
    }

    public String getOwnerName() {
        return getString(OWNERNAME);
    }

    public UUID getHorseUuid() {
        return UUID.fromString(getString(HORSEUUID));
    }

    public String getHorseName() {
        return getString(HORSENAME);
    }

    public String getVariant() {
        return getString(VARIANT);
    }

    public Double getJumpValue() {
        return getDouble(VALUE_JUMP);
    }

    public Double getSpeedValue() {
        return getDouble(VALUE_SPEED);
    }

    public Double getHealthValue() {
        return getDouble(VALUE_HEALTH);
    }

    public String getHorseType() {
        return getString(ENTITYTYPE);
    }


    public void updateOwner(UUID receiverUuid, String receiverName) {
        setString(OWNERUUID, receiverUuid.toString());
        setString(OWNERNAME, receiverName);

        setLore();
    }

    // HELPER

    private void setLore() {
        ItemMeta itemMeta = getItem().getItemMeta();
        ArrayList<String> lore = new ArrayList<>();
        lore.add("Name: " + getHorseName());
        lore.add("Owner: " + getOwnerName());
        itemMeta.setLore(lore);
        setItemMeta(itemMeta);
    }

    private ItemMeta addGlow(ItemMeta itemMeta) {
        itemMeta.addEnchant(Enchantment.DURABILITY, 1, true);
        itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        return itemMeta;
    }

    private static String getCarpetColorAsString(short color) {
        List<String> colorNames = new ArrayList<>();
        colorNames.add(0, "WHITE");
        colorNames.add(1, "ORANGE");
        colorNames.add(2, "MAGENTA");
        colorNames.add(3, "LIGHT_BLUE");
        colorNames.add(4, "YELLOW");
        colorNames.add(5, "LIME");
        colorNames.add(6, "PINK");
        colorNames.add(7, "GRAY");
        colorNames.add(8, "SILVER");
        colorNames.add(9, "CYAN");
        colorNames.add(10, "PURPLE");
        colorNames.add(11, "BLUE");
        colorNames.add(12, "BROWN");
        colorNames.add(13, "GREEN");
        colorNames.add(14, "RED");
        colorNames.add(15, "BLACK");

        return colorNames.get(color);
    }

    private static short getCarpetColorAsShort(String color) {
        for (short i = 0; i < 16; i++) {
            if (getCarpetColorAsString(i).equalsIgnoreCase(color)) {
                return i;
            }
        }

        return 0;
    }

    private static String translateOldVariants(String variant) {
        switch (variant) {
            case "DONKEY":
                return "Horse";
            case "HORSE":
                return "Horse";
            case "MULE":
                return "Mule";
            case "SKELETON_HORSE":
                return "SkeletonHorse";
            default:
                return variant;
        }
    }

}
