package de.cubenation.cnhorses.messages;

import de.cubenation.bedrock.helper.MessageHelper;
import de.cubenation.bedrock.translation.JsonMessage;
import de.cubenation.cnhorses.CNHorses;
import net.md_5.bungee.api.ChatMessageType;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.*;

import java.util.ArrayList;

/**
 * Created by bhruschka on 28.01.17.
 * Project: CNHorses
 */
public class Messages extends MessageHelper {

    private static CNHorses plugin = CNHorses.getInstance();

    public static void InventoryIsFull(Player player, AbstractHorse abstractHorse) {
        new JsonMessage(plugin, "inventory.full", "type", getHorseType(abstractHorse)).send(player);
    }

    public static void StrangerHorse(Player player, AbstractHorse abstractHorse) {
        String horseType = getHorseType(abstractHorse);

        if (abstractHorse instanceof Donkey) {
            new JsonMessage(plugin, "stranger.horse.donkey", "type", horseType).send(player);
        } else {
            new JsonMessage(plugin, "stranger.horse.default", "type", horseType).send(player);
        }
    }

    public static void StrangerHorse(Player player, String variant) {
        String horseType = getHorseType(variant);

        if (variant != null && (variant.equalsIgnoreCase("DONKEY") || variant.equalsIgnoreCase("MULE"))) {
            new JsonMessage(plugin, "stranger.horse.donkey", "type", horseType).send(player);
        } else {
            new JsonMessage(plugin, "stranger.horse.default", "type", horseType).send(player);
        }
    }

    // Helper

    private static String getHorseType(AbstractHorse abstractHorse) {
        if (abstractHorse instanceof Horse) {
            return getPlainText(plugin, "entity.type.horse");
        } else if (abstractHorse instanceof Llama) {
            return getPlainText(plugin, "entity.type.llama");
        } else if (abstractHorse instanceof Donkey) {
            return getPlainText(plugin, "entity.type.donkey");
        } else if (abstractHorse instanceof Mule) {
            return getPlainText(plugin, "entity.type.mule");
        }
        return getPlainText(plugin, "entity.type.animal");
    }

    private static String getHorseType(String variant) {
        if (variant == null) {
            return getPlainText(plugin, "entity.type.animal");
        }

        if (variant.equalsIgnoreCase("DONKEY")) {
            return getPlainText(plugin, "entity.type.donkey");
        } else if (variant.equalsIgnoreCase("MULE")) {
            return getPlainText(plugin, "entity.type.mule");
        } else if (variant.equalsIgnoreCase("HORSE")) {
            return getPlainText(plugin, "entity.type.horse");
        } else if (variant.equalsIgnoreCase("SKELETON_HORSE")) {
            return getPlainText(plugin, "entity.type.skeletonhorse");
        } else if (variant.equalsIgnoreCase("LLAMA")) {
            return getPlainText(plugin, "entity.type.llama");
        } else {
            return getPlainText(plugin, "entity.type.animal");
        }
    }

    public static void TransferSuccess(Player player, String oldOwner, String newOwner, String variant) {
        String oldOwnerName = oldOwner;
        if (oldOwner.equalsIgnoreCase(player.getDisplayName())) {
            oldOwnerName = getPlainText(plugin, "plaintext.you");
        }

        new JsonMessage(plugin, "success.transfer", "animal", getHorseType(variant), "oldowner", oldOwnerName, "newowner", newOwner).send(player);
    }

    public static class Worlds {
        public static void error(CommandSender commandSender, World world) {
            error(commandSender, world.getName());
        }

        public static void error(CommandSender commandSender, String world) {
            new JsonMessage(plugin, "world.error", "world", world).send(commandSender);
        }

        public static void addSuccessful(CommandSender commandSender, World world) {
            addSuccessful(commandSender, world.getName());
        }

        public static void addSuccessful(CommandSender commandSender, String world) {
            new JsonMessage(plugin, "world.add", "world", world).send(commandSender);
        }

        public static void removeSuccessful(CommandSender commandSender, World world) {
            removeSuccessful(commandSender, world.getName());
        }

        public static void removeSuccessful(CommandSender commandSender, String world) {
            new JsonMessage(plugin, "world.remove", "world", world).send(commandSender);
        }

        public static void configContainsAlready(CommandSender commandSender, World world) {
            configContainsAlready(commandSender, world.getName());
        }

        public static void configContainsAlready(CommandSender commandSender, String world) {
            new JsonMessage(plugin, "world.list.contains.already", "world", world).send(commandSender);
        }

        public static void configContainsNot(CommandSender commandSender, World world) {
            configContainsNot(commandSender, world.getName());
        }

        public static void configContainsNot(CommandSender commandSender, String world) {
            new JsonMessage(plugin, "world.list.contains.not", "world", world).send(commandSender);
        }

        public static void listEmpty(CommandSender commandSender) {
            new JsonMessage(plugin, "world.list.empty").send(commandSender);
        }

        public static void list(CommandSender commandSender, ArrayList<World> enabledWorlds) {
            new JsonMessage(plugin, "world.list.header.info").send(commandSender);

            for (World enabledWorld : enabledWorlds) {
                new JsonMessage(plugin, "world.list.entry.info", "world", enabledWorld.getName()).send(commandSender);
            }
        }
    }

    public static class Bypass {

        public static void Info(Player player) {
            new JsonMessage(plugin, "bypass.used").send(player, ChatMessageType.ACTION_BAR);
        }

    }

    public static class Error {

        public static void Default(Player player) {
            new JsonMessage(plugin, "error.default").send(player);
        }

        public static void LocationNotEnabled(Player player) {
            new JsonMessage(plugin, "location.disabled", "plugin", "CNHorses").send(player);
        }

        public static void EmptyHand(Player player) {
            new JsonMessage(plugin, "error.empty.hand").send(player);
        }

        public static void WrongSaddle(Player player) {
            new JsonMessage(plugin, "error.wrong.saddle").send(player);
        }

        public static void InvalidSaddle(Player player) {
            new JsonMessage(plugin, "error.invalid.saddle").send(player);
        }

        public static void PlayerNotFound(Player player, String name) {
            new JsonMessage(plugin, "error.player.not.found", "name", name).send(player);
        }

        public static void PlayerForUUIDNotFound(Player player, String uuid) {
            new JsonMessage(plugin, "error.uuid.not.found", "uuid", uuid).send(player);
        }
    }

    public static class Confirm {

        public static void Timeout(CommandSender sender) {
            new JsonMessage(plugin, "confirm.timeout").send(sender);
        }

        public static void Nothing(Player player) {
            new JsonMessage(plugin, "confirm.nothing").send(player);
        }

        public static void Transfer(Player player) {
            String commandinfo = getPlainText(plugin, "plaintext.confirm.command.info", "command", "/horses confirm");
            new JsonMessage(plugin, "confirm.transfer", "command", "/horses confirm", "commandinfo", commandinfo).send(player);
        }
    }

    public static class Info {

        public static void DropSaddle(Player player, String variant) {
            String text = getPlainText(plugin, "plaintext.drop.saddle", "variant", getHorseType(variant));
            player.sendTitle("", text, 10, 70, 20);
        }

    }
}
