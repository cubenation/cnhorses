package de.cubenation.cnhorses;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.flags.registry.FlagConflictException;
import com.sk89q.worldguard.protection.flags.registry.FlagRegistry;
import de.cubenation.bedrock.BasePlugin;
import de.cubenation.bedrock.command.CommandRole;
import de.cubenation.bedrock.exception.NoSuchPluginException;
import de.cubenation.bedrock.reloadable.Reloadable;
import de.cubenation.cnhorses.command.*;
import de.cubenation.cnhorses.config.HorseConfig;
import de.cubenation.cnhorses.config.WorldConfig;
import de.cubenation.cnhorses.config.locale.de_DE;
import de.cubenation.cnhorses.listener.EntityListener;
import de.cubenation.cnhorses.listener.InventoryListener;
import de.cubenation.cnhorses.listener.PlayerListener;
import de.cubenation.cnhorses.manager.HorseManager;
import de.cubenation.cnhorses.manager.WorldManager;
import de.cubenation.cnhorses.util.CsvFileWriter;
import de.cubenation.cnhorses.util.HorseSaddleLogEntry;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;

/**
 * Created by bhruschka on 28.01.17.
 * Project: CNHorses
 */
public class CNHorses extends BasePlugin {

    public static final StateFlag HORSES_STORE = new StateFlag("horses-store", true);
    public static final StateFlag HORSES_PLACE = new StateFlag("horses-place", true);

    public static CNHorses instance;

    public static CNHorses getInstance() {
        return CNHorses.instance;
    }

    private void setInstance(CNHorses instance) {
        CNHorses.instance = instance;
    }

    private ArrayList<HorseSaddleLogEntry> horseSaddleLogs = new ArrayList<>();

    private DateFormat formatter = new SimpleDateFormat("yyyy-MM-d");


    @Override
    public void onLoad() {
        super.onLoad();

        try {
            WorldGuardPlugin worldGuardPlugin = getWorldGuardPlugin();

            FlagRegistry flagRegistry = worldGuardPlugin.getFlagRegistry();
            try {
                // register our flag with the registry
                flagRegistry.register(HORSES_STORE);
                flagRegistry.register(HORSES_PLACE);
            } catch (FlagConflictException e) {
                log(Level.WARNING, "Can't register CNHorses custom Worldguard Flags!");
            }

        } catch (NoSuchPluginException e) {
            // Softdependency -> ignore
        }
    }

    @Override
    protected void onPreEnable() throws Exception {
        setInstance(this);
    }

    @Override
    protected void onPostEnable() throws Exception {
        getServer().getPluginManager().registerEvents(new InventoryListener(), this);
        getServer().getPluginManager().registerEvents(new PlayerListener(), this);
        getServer().getPluginManager().registerEvents(new EntityListener(), this);

        this.getPermissionService().registerPermission(HorseManager.horse_place, CommandRole.USER);
        this.getPermissionService().registerPermission(HorseManager.horse_place_other, CommandRole.MODERATOR);
        this.getPermissionService().registerPermission(HorseManager.horse_store, CommandRole.USER);
        this.getPermissionService().registerPermission(HorseManager.horse_store_other, CommandRole.MODERATOR);

        this.getPermissionService().registerPermission(HorseManager.bypass_horse_place, CommandRole.MODERATOR);
        this.getPermissionService().registerPermission(HorseManager.bypass_horse_store, CommandRole.MODERATOR);

        this.getPermissionService().registerPermission(InventoryListener.bypass_inventory_other, CommandRole.MODERATOR);

        scheduleTasks();
    }

    @Override
    public void onDisable() {
        super.onDisable();

        CsvFileWriter.logToFile(getDataFolder(), formatter.format(new Date()), horseSaddleLogs);
    }

    @Override
    public void setCommands(HashMap<String, ArrayList<Class<?>>> commands) {
        commands.put("horse", new ArrayList<Class<?>>() {{
            add(TransferCommand.class);
            add(ConfirmCommand.class);

            add(AddWorldCommand.class);
            add(RemoveWorldCommand.class);
            add(ListWorldCommand.class);
        }});
    }

    @Override
    public ArrayList<Class<?>> getCustomConfigurationFiles() {
        return new ArrayList<Class<?>>() {{
            add(de_DE.class);
            add(HorseConfig.class);
            add(WorldConfig.class);
        }};
    }

    @Override
    public ArrayList<Reloadable> getReloadable() {
        return new ArrayList<Reloadable>() {{
            add(WorldManager.getInstance());
        }};
    }

    private void scheduleTasks() {
        // LogSaver every 10 mins
        new BukkitRunnable() {
            @Override
            public void run() {
                // Log payout to File
                if (horseSaddleLogs.isEmpty()) {
                    return;
                }
                CsvFileWriter.logToFile(getDataFolder(), formatter.format(new Date()), horseSaddleLogs);
                horseSaddleLogs.clear();
            }
        }.runTaskTimer(this, 20 * 60 * 10, 20 * 60 * 10);
    }

    /**
     * Return the WorldGuard plugin instance
     *
     * @return WorldGuardPlugin
     * @throws NoSuchPluginException
     */
    public WorldGuardPlugin getWorldGuardPlugin() throws NoSuchPluginException {
        return (WorldGuardPlugin) getPlugin("WorldGuard");
    }

    public void logBrokenOwner(Player player, String info) {
        log(Level.INFO, info + ": " + player.getName() + " stored a horse with a broken Owner. " + player.getWorld().getName());

    }

    public void addHorseSaddleLogs(HorseSaddleLogEntry entry) {
        horseSaddleLogs.add(entry);
    }
}
