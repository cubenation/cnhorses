package de.cubenation.cnhorses.config;

import de.cubenation.bedrock.BasePlugin;
import de.cubenation.bedrock.service.config.CustomConfigurationFile;
import net.cubespace.Yamler.Config.Path;

import java.io.File;

/**
 * Created by bhruschka on 28.01.17.
 * Project: CNHorses
 */
public class HorseConfig extends CustomConfigurationFile {

    public static String getFilename() {
        return "horses.yaml";
    }

    public HorseConfig(BasePlugin plugin) {
        this(plugin, getFilename());
    }

    public HorseConfig(BasePlugin plugin, String filename) {
        CONFIG_FILE = new File(plugin.getDataFolder(), filename);
    }

    @Path("store.shiftClickIgnored")
    private boolean storeShiftClickIgnored = true;

    public boolean isStoreShiftClickIgnored() {
        return storeShiftClickIgnored;
    }

    @Path("dismount.onlogout")
    private boolean dismountOnLogout = true;

    public boolean isDismountOnLogout() {
        return dismountOnLogout;
    }

    @Path("enable.store")
    private boolean enableStore = true;

    public boolean isStoreEnabled() {
        return enableStore;
    }

    @Path("enable.place")
    private boolean enablePlace = true;

    public boolean isPlaceEnabled() {
        return enablePlace;
    }

    @Path("protect.horse.inventory")
    private boolean protectHorseInventory = true;

    public boolean isProtectHorseInventoryEnabled() {
        return protectHorseInventory;
    }

    @Path("protect.horse.attack")
    private boolean protectHorseAttack = true;

    public boolean isProtectHorseAttackEnabled() {
        return protectHorseAttack;
    }

    @Path("deny.horse.in.portableinv")
    private boolean denyHorseInPortableInv = true;

    public boolean isDenyHorseInPortableInv() {
        return denyHorseInPortableInv;
    }

    @Path("deny.shulkerbox.in.portableinv")
    private boolean denyShulkerboxInPortableInv = true;

    public boolean isDenyShulkerboxInPortableInv() {
        return denyShulkerboxInPortableInv;
    }
}
