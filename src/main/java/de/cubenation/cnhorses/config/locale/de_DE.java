package de.cubenation.cnhorses.config.locale;

import de.cubenation.bedrock.BasePlugin;
import de.cubenation.bedrock.service.config.CustomConfigurationFile;
import net.cubespace.Yamler.Config.Comment;
import net.cubespace.Yamler.Config.Path;

import java.io.File;
import java.util.HashMap;

/**
 * Created by bhruschka on 28.01.17.
 * Project: CNHorses
 */
public class de_DE extends CustomConfigurationFile {

    public static String getFilename() {
        return "locale" + File.separator + "de_DE.yml";
    }

    public de_DE(BasePlugin plugin) {
        CONFIG_FILE = new File(plugin.getDataFolder(), getFilename());
    }

    // Commands

    @Path("command.transfer.horse.desc")
    private String command_transfer_horse_desc = "Übertrage dein Pferd/Lama an einen anderen Spieler.";

    @Path("command.arg.name")
    private HashMap<String, String> command_arg_name = new HashMap<String, String>() {{
        put("desc", "Name oder UUID Spielers.");
        put("ph", "Spielername/UUID");
    }};

    @Path("command.confirm.desc")
    private String command_confirm_desc = "Bestätige das Übertragen des Pferds/Lamas.";

    @Path("command.worlds.add.desc")
    private String command_worlds_add_desc = "Aktiviere eine Welt für die CNHorses Nutzung.";

    @Path("command.worlds.remove.desc")
    private String command_worlds_remove_desc = "Deaktiviere eine Welt für die CNHorses Nutzung.";

    @Path("command.worlds.add.args.world")
    private HashMap<String, String> commandWorldsAddArgsWorld = new HashMap<String, String>() {{
        put("desc", "Name der Welt");
        put("ph", "Welt");
    }};

    @Path("command.worlds.list.desc")
    private String command_worlds_list_desc = "Zeigt eine Liste der Welten, in denen CNHorses genutzt werden können.";




    @Path("inventory.full")
    private String inventoryFull = "{\"text\":\"\",\"extra\":[{\"text\":\"%plugin_prefix%\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"Dein Inventar ist voll. Du kannst das %type% nicht verstauen.\",\"color\":\"&TEXT&\"}]}";

    @Path("stranger.horse.default")
    private String strangerHorseDefault = "{\"text\":\"\",\"extra\":[{\"text\":\"%plugin_prefix%\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"Dieses %type% gehört dir nicht!\",\"color\":\"&TEXT&\"}]}";

    @Path("stranger.horse.donkey")
    private String strangerHorseDonkey = "{\"text\":\"\",\"extra\":[{\"text\":\"%plugin_prefix%\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"Dieser %type% gehört dir nicht!\",\"color\":\"&TEXT&\"}]}";

    @Path("bypass.used")
    private String bypassUsed = "{\"text\":\"\",\"extra\":[{\"text\":\"%plugin_prefix%\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"Bypass genutzt.\",\"color\":\"&PRIMARY&\"}]}";


    // Confirm

    @Comment("Args: %commandinfo%")
    @Path("confirm.transfer")
    private String confirmTransfer = "{\"text\":\"\",\"extra\":[{\"text\":\"%plugin_prefix%\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"Klicke hier, um das Übertragen zu bestätigen.\",\"color\":\"&SECONDARY&\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"%command%\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"%commandinfo%\"}}]}";

    @Path("confirm.timeout")
    private String confirmTimeout = "{\"text\":\"\",\"extra\":[{\"text\":\"%plugin_prefix%\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"Du warst leider nicht schnell genug. Bitte versuche es erneut.\",\"color\":\"&SECONDARY&\"}]}";

    @Path("confirm.nothing")
    private String confirmNothing = "{\"text\":\"\",\"extra\":[{\"text\":\"%plugin_prefix%\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"Du hast nichts zum bestätigen!\",\"color\":\"&SECONDARY&\"}]}";


    // World

    @Path("world.add")
    private String world_add = "{\"text\":\"\",\"extra\":[{\"text\":\"%plugin_prefix%\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"Welt:\",\"color\":\"&WHITE&\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"%world%\",\"color\":\"&SECONDARY&\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"wurden aktiviert.\",\"color\":\"&WHITE&\"}]}";

    @Path("world.remove")
    private String world_remove = "{\"text\":\"\",\"extra\":[{\"text\":\"%plugin_prefix%\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"Welt:\",\"color\":\"&WHITE&\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"%world%\",\"color\":\"&SECONDARY&\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"wurden deaktiviert.\",\"color\":\"&WHITE&\"}]}";

    @Path("world.error")
    private String world_error = "{\"text\":\"\",\"extra\":[{\"text\":\"%plugin_prefix%\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"Fehler beim editieren der Config für Welt:\",\"color\":\"&WHITE&\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"%world%\",\"color\":\"&SECONDARY&\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\". \",\"color\":\"&WHITE&\"}]}";

    @Path("world.list.contains.already")
    private String world_list_contains_already = "{\"text\":\"\",\"extra\":[{\"text\":\"%plugin_prefix%\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"Welt:\",\"color\":\"&WHITE&\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"%world%\",\"color\":\"&SECONDARY&\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"ist schon aktiviert!\",\"color\":\"&WHITE&\"}]}";

    @Path("world.list.contains.not")
    private String world_list_contains_not = "{\"text\":\"\",\"extra\":[{\"text\":\"%plugin_prefix%\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"Welt:\",\"color\":\"&WHITE&\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"%world%\",\"color\":\"&SECONDARY&\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"ist schon deaktiviert!\",\"color\":\"&WHITE&\"}]}";

    @Path("world.list.empty")
    private String world_list_empty = "{\"text\":\"\",\"extra\":[{\"text\":\"%plugin_prefix%\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"Zurzeit gibt es keine aktiven Welten.\",\"color\":\"&TEXT&\"}]}";

    @Path("world.list.header.info")
    private String world_list_header_info = "{\"text\":\"\",\"extra\":[{\"text\":\"%plugin_prefix%\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"Folgende Welten wurden gefunden:\",\"color\":\"&TEXT&\"}]}";

    @Path("world.list.entry.info")
    private String world_list_entry_info = "{\"text\":\"* \",\"color\":\"&SECONDARY&\",\"extra\":[{\"text\":\" \",\"color\":\"white\"},{\"text\":\"%world%\",\"color\":\"&WHITE&\"}]}";

    @Path("location.disabled")
    private String locationDisabled = "{\"text\":\"\",\"extra\":[{\"text\":\"%plugin_prefix%\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"Hier ist die Nutzung von %plugin% nicht erlaubt.\",\"color\":\"&TEXT&\"}]}";

    @Comment("Args: %animal%, %oldowner%, %newowner%")
    @Path("success.transfer")
    private String successTransfer = "{\"text\":\"\",\"extra\":[{\"text\":\"%plugin_prefix%\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"Woop Dee Wooooo!\",\"color\":\"yellow\"},{\"text\":\"\\n\",\"color\":\"white\"},{\"text\":\"Das %animal% gehört jetzt nicht mehr %oldowner%, sondern %newowner%.\",\"color\":\"&TEXT&\"}]}";


    @Path("error.player.not.found")
    private String error_player_not_found = "{\"text\":\"\",\"extra\":[{\"text\":\"%plugin_prefix%\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"Der Spieler\",\"color\":\"&TEXT&\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"%name%\",\"color\":\"&SECONDARY&\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"konnte nicht gefunden werden. \",\"color\":\"&TEXT&\"}]}";

    @Path("error.uuid.not.found")
    private String error_uuid_not_found = "{\"text\":\"\",\"extra\":[{\"text\":\"%plugin_prefix%\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"Der Spieler für die UUID:\",\"color\":\"&TEXT&\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"%uuid%\",\"color\":\"&SECONDARY&\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"konnte nicht gefunden werden. \",\"color\":\"&TEXT&\"}]}";

    @Path("error.invalid.saddle")
    private String error_invalid_saddle = "{\"text\":\"\",\"extra\":[{\"text\":\"%plugin_prefix%\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"Das ist kein CNHorses Sattel!\",\"color\":\"&TEXT&\"}]}";

    @Path("error.wrong.saddle")
    private String error_wrong_saddle = "{\"text\":\"\",\"extra\":[{\"text\":\"%plugin_prefix%\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"Das ist der falsche Sattel!\",\"color\":\"&TEXT&\"}]}";

    @Path("error.empty.hand")
    private String error_empty_hand = "{\"text\":\"\",\"extra\":[{\"text\":\"%plugin_prefix%\"},{\"text\":\" \",\"color\":\"white\"},{\"text\":\"Dafür musst du ein Item in die Hand nehmen.\",\"color\":\"&TEXT&\"}]}";


    // Plaintext

    @Path("entity.type")
    private HashMap<String, String> entityType = new HashMap<String, String>() {{
        put("animal", "Reittier");
        put("horse", "Pferd");
        put("llama", "Lama");
        put("donkey", "Esel");
        put("mule", "Maultier");
        put("skeletonhorse", "Skelet-Pfer");
    }};

    @Comment("Args: %command%")
    @Path("plaintext.confirm.command.info")
    private String plaintext_confirm_command_info = "Oder schreibe %command%";

    @Path("plaintext.you")
    private String plaintext_you = "dir";

    @Path("plaintext.drop.saddle")
    private String plaintextDropSaddle = "Du hat gerade dein %variant% fallen gelassen!";

}
