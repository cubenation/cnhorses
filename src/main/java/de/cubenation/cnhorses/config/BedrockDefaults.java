package de.cubenation.cnhorses.config;

import de.cubenation.bedrock.BasePlugin;

import java.io.File;

/**
 * Created by bhruschka on 28.01.17.
 * Project: CNHorses
 */

public class BedrockDefaults extends de.cubenation.bedrock.config.BedrockDefaults {

    public BedrockDefaults(BasePlugin plugin) {
        CONFIG_FILE = new File(plugin.getDataFolder(), de.cubenation.bedrock.config.BedrockDefaults.getFilename());
        CONFIG_HEADER = getHeader();

        this.setColorSchemeName("YELLOW");

        this.setLocalizationLocale("de_DE");
    }

}

