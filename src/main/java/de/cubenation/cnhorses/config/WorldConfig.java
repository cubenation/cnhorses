package de.cubenation.cnhorses.config;

import de.cubenation.bedrock.BasePlugin;
import de.cubenation.bedrock.exception.ConfigAlreadyContainsObjectException;
import de.cubenation.bedrock.exception.ConfigNotContainsObjectException;
import de.cubenation.bedrock.service.config.CustomConfigurationFile;
import de.cubenation.cnhorses.manager.WorldManager;
import net.cubespace.Yamler.Config.InvalidConfigurationException;
import net.cubespace.Yamler.Config.Path;
import org.bukkit.Bukkit;
import org.bukkit.World;

import java.io.File;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by bhruschka on 29.01.17.
 * Project: CNHorses
 */
public class WorldConfig extends CustomConfigurationFile {

    public static String getFilename() {
        return "worlds.yaml";
    }

    public WorldConfig(BasePlugin plugin) {
        this(plugin, getFilename());
    }

    public WorldConfig(BasePlugin plugin, String filename) {
        CONFIG_FILE = new File(plugin.getDataFolder(), filename);
    }

    @Path("enabled.worlds")
    private ArrayList<String> enabledWorlds = new ArrayList<>();

    private ArrayList<String> getEnabledWorlds() {
        return enabledWorlds;
    }

    public boolean storeWorld(World world) throws ConfigAlreadyContainsObjectException {
        if (getWorlds().contains(world)) {
            throw new ConfigAlreadyContainsObjectException();
        }

        enabledWorlds.add(world.getUID().toString());

        try {
            this.save();
            this.reload();

            WorldManager.getInstance().reload();

            return true;
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean removeWorld(World world) throws ConfigNotContainsObjectException {

        if (!getWorlds().contains(world)) {
            throw new ConfigNotContainsObjectException();
        }

        enabledWorlds.remove(world.getUID().toString());

        try {
            this.save();
            this.reload();

            WorldManager.getInstance().reload();

            return true;
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
            return false;
        }
    }

    public ArrayList<World> getWorlds() {
        ArrayList<World> worldList = new ArrayList<>();
        for (String world : this.enabledWorlds) {
            World bukkitWorld = Bukkit.getServer().getWorld(UUID.fromString(world));
            if (bukkitWorld != null) {
                worldList.add(bukkitWorld);
            }
        }
        return worldList;
    }
}
