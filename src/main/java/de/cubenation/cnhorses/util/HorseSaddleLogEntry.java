package de.cubenation.cnhorses.util;

import java.util.Date;
import java.util.UUID;

/**
 * Created by bhruschka on 05.02.17.
 * Project: CNHorses
 */
public class HorseSaddleLogEntry {

    private Date date;

    private UUID ownerUuid;

    private String ownerName;

    private String horseName;

    private UUID horseUuid;

    private double jumpValue;

    private double speedValue;

    private double healthValue;

    private String horseType;

    private String horseVariant;

    private String world;

    private Integer x;

    private Integer y;

    private Integer z;

    private String type;

    private Integer inventorySize;


    public HorseSaddleLogEntry(UUID ownerUuid, String ownerName, String horseName, UUID horseUuid, double jumpValue, double speedValue, double healthValue, String horseType, String horseVariant, String world, Integer x, Integer y, Integer z, String type, Integer inventorySize) {
        this.date = new Date();
        this.ownerUuid = ownerUuid;
        this.ownerName = ownerName;
        this.horseName = horseName;
        this.horseUuid = horseUuid;
        this.jumpValue = jumpValue;
        this.speedValue = speedValue;
        this.healthValue = healthValue;
        this.horseType = horseType;
        this.horseVariant = horseVariant;
        this.world = world;
        this.x = x;
        this.y = y;
        this.z = z;
        this.type = type;
        this.inventorySize = inventorySize;
    }

    public Date getDate() {
        return date;
    }

    public UUID getOwnerUuid() {
        return ownerUuid;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public String getHorseName() {
        return horseName;
    }

    public UUID getHorseUuid() {
        return horseUuid;
    }

    public double getJumpValue() {
        return jumpValue;
    }

    public double getSpeedValue() {
        return speedValue;
    }

    public double getHealthValue() {
        return healthValue;
    }

    public String getHorseType() {
        return horseType;
    }

    public String getHorseVariant() {
        return horseVariant;
    }

    public String getWorld() {
        return world;
    }

    public Integer getX() {
        return x;
    }

    public Integer getY() {
        return y;
    }

    public Integer getZ() {
        return z;
    }

    public String getType() {
        return type;
    }

    public Integer getInventorySize() {
        return inventorySize;
    }
}
