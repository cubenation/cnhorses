package de.cubenation.cnhorses.util;

import de.cubenation.bedrock.BasePlugin;
import de.cubenation.bedrock.exception.TimeoutException;
import de.cubenation.bedrock.service.confirm.AbstractConfirmService;
import de.cubenation.bedrock.service.confirm.ConfirmRegistry;
import de.cubenation.cnhorses.manager.HorseManager;
import de.cubenation.cnhorses.messages.Messages;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by bhruschka on 30.01.17.
 * Project: CNHorses
 */
public class ConfirmHorseTransfer extends AbstractConfirmService {

    public ConfirmHorseTransfer(BasePlugin plugin) {
        super(plugin);
    }

    @Override
    public void call() {

        // cancel timeout info task
        this.task.cancel();

        // check timeout
        try {
            this.checkExceeded();
        } catch (TimeoutException e) {
            return;
        }

        CommandSender sender = (CommandSender) this.get("sender").get();
        Player player = (Player) sender;
        UUID horseUuid = (UUID) this.get("horseUuid").get();
        UUID receiverUuid = (UUID) this.get("receiverUuid").get();
        String receiverName = (String) this.get("receiverName").get();

        HorseManager.getInstance().performTransfer(player, receiverUuid, receiverName, horseUuid);
    }

    @Override
    public void abort() {
        CommandSender sender = (CommandSender) this.get("sender").get();
        ConfirmRegistry.getInstance().remove(sender);
        Messages.Confirm.Timeout(sender);
    }

}
