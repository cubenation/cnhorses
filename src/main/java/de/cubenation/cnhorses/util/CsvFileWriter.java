package de.cubenation.cnhorses.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Created by bhruschka on 05.02.17.
 * Project: CNHorses
 */
public class CsvFileWriter {

    // Header
    private static final String HEADER = "timestamp,ownerUuid,ownerName,horseName,horseUuid,jump,speed,halth,horsetype,horsevariant,world,x,y,z,type,items";

    // Delimiter used in CSV file
    private static final String COMMA_DELIMITER = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";


    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static void logToFile(File dataFolder, String filename, ArrayList<HorseSaddleLogEntry> entries) {
        if (entries == null) {
            return;
        }
        if (entries.isEmpty()) {
            return;
        }

        try {

            if (!dataFolder.exists()) {
                dataFolder.mkdir();
            }

            File logs = new File(dataFolder, "logs");
            if (!logs.exists()) {
                logs.mkdir();
            }

            Boolean header = false;
            File saveTo = new File(logs, filename + ".csv");
            if (!saveTo.exists()) {
                saveTo.createNewFile();
                header = true;
            }


            FileWriter fileWriter = new FileWriter(saveTo, true);
            PrintWriter pw = new PrintWriter(fileWriter);

            if (header) {
                pw.append(HEADER);
                pw.append(NEW_LINE_SEPARATOR);
            }

            for (HorseSaddleLogEntry entry : entries) {
                pw.append(String.valueOf(entry.getDate().getTime()));
                pw.append(COMMA_DELIMITER);
                pw.append(String.valueOf(entry.getOwnerUuid()));
                pw.append(COMMA_DELIMITER);
                pw.append(String.valueOf(entry.getOwnerName()));
                pw.append(COMMA_DELIMITER);
                pw.append(String.valueOf(entry.getHorseName()));
                pw.append(COMMA_DELIMITER);
                pw.append(String.valueOf(entry.getHorseUuid()));
                pw.append(COMMA_DELIMITER);
                pw.append(String.valueOf(entry.getJumpValue()));
                pw.append(COMMA_DELIMITER);
                pw.append(String.valueOf(entry.getSpeedValue()));
                pw.append(COMMA_DELIMITER);
                pw.append(String.valueOf(entry.getHealthValue()));
                pw.append(COMMA_DELIMITER);
                pw.append(String.valueOf(entry.getHorseType()));
                pw.append(COMMA_DELIMITER);
                pw.append(String.valueOf(entry.getHorseVariant()));
                pw.append(COMMA_DELIMITER);
                pw.append(String.valueOf(entry.getWorld()));
                pw.append(COMMA_DELIMITER);
                pw.append(String.valueOf(entry.getX()));
                pw.append(COMMA_DELIMITER);
                pw.append(String.valueOf(entry.getY()));
                pw.append(COMMA_DELIMITER);
                pw.append(String.valueOf(entry.getZ()));
                pw.append(COMMA_DELIMITER);
                pw.append(String.valueOf(entry.getType()));
                pw.append(COMMA_DELIMITER);
                pw.append(String.valueOf(entry.getInventorySize()));

                pw.append(NEW_LINE_SEPARATOR);
            }

            pw.flush();
            pw.close();

        } catch (IOException e) {

            e.printStackTrace();

        }

    }

}
