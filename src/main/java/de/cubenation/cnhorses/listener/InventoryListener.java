package de.cubenation.cnhorses.listener;

import de.cubenation.cnhorses.CNHorses;
import de.cubenation.cnhorses.config.HorseConfig;
import de.cubenation.cnhorses.manager.HorseManager;
import de.cubenation.cnhorses.manager.InventoryManager;
import de.cubenation.cnhorses.messages.Messages;
import org.bukkit.entity.AbstractHorse;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.HorseInventory;
import org.bukkit.inventory.LlamaInventory;

/**
 * Created by bhruschka on 28.01.17.
 * Project: CNHorses
 */

@SuppressWarnings("unused")
public class InventoryListener implements Listener {

    public static String bypass_inventory_other = "bypass.inventory.other";

    private final CNHorses plugin = CNHorses.getInstance();

    @EventHandler
    public void onClickEvent(InventoryClickEvent event) {
        boolean toCancel = HorseManager.getInstance().handleHorseStore(event);
        if (toCancel) {
            event.setCancelled(true);
            return;
        }

        HorseConfig horseConfig = (HorseConfig) plugin.getConfigService().getConfig(HorseConfig.class);

        if (horseConfig.isDenyHorseInPortableInv()) {
            boolean preventHorseInPortableInventory = InventoryManager.getInstance().preventHorseInPortableInventory(event);
            if (preventHorseInPortableInventory) {
                event.setCancelled(true);
                return;
            }
        }

        if (horseConfig.isDenyShulkerboxInPortableInv()) {
            boolean preventShulkerInPortableInventory = InventoryManager.getInstance().preventShulkerInPortableInventory(event);
            if (preventShulkerInPortableInventory) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onInventoryOpenEvent(InventoryOpenEvent event) {
        if ((!(event.getInventory() instanceof HorseInventory)) && !(event.getInventory() instanceof LlamaInventory)) {
            return;
        }

        if (!(event.getInventory().getHolder() instanceof AbstractHorse)) {
            return;
        }

        HorseConfig horseConfig = (HorseConfig) plugin.getConfigService().getConfig(HorseConfig.class);
        if (!horseConfig.isProtectHorseInventoryEnabled()) {
            return;
        }

        if (!(event.getPlayer() instanceof Player)) {
            return;
        }

        Player player = (Player) event.getPlayer();
        AbstractHorse abstractHorse = (AbstractHorse) event.getInventory().getHolder();

        if (!abstractHorse.getOwner().getUniqueId().equals(player.getUniqueId())) {
            if (!plugin.getPermissionService().hasPermission(player, bypass_inventory_other)) {
                Messages.StrangerHorse(player, abstractHorse);
                event.setCancelled(true);
            } else {
                Messages.Bypass.Info(player);
            }
        }
    }

}
