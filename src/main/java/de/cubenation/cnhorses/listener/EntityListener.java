package de.cubenation.cnhorses.listener;

import de.cubenation.cnhorses.CNHorses;
import de.cubenation.cnhorses.config.HorseConfig;
import org.bukkit.entity.AbstractHorse;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

import java.util.ArrayList;

import static org.bukkit.event.entity.EntityDamageEvent.DamageCause.*;

/**
 * Created by bhruschka on 01.02.17.
 * Project: CNHorses
 */

@SuppressWarnings("unused")
public class EntityListener implements Listener {

    private final CNHorses plugin = CNHorses.getInstance();

    private final ArrayList<EntityDamageEvent.DamageCause> damageCauses = new ArrayList<EntityDamageEvent.DamageCause>() {{
        add(ENTITY_ATTACK);
        add(ENTITY_SWEEP_ATTACK);
        add(PROJECTILE);
    }};

    @EventHandler
    public void onEntityDamageEvent(EntityDamageEvent event) {
        if (!(event.getEntity() instanceof AbstractHorse)) {
            return;
        }

        if (event.getEntity().getPassengers() == null || event.getEntity().getPassengers().isEmpty()) {
            return;
        }

        HorseConfig horseConfig = (HorseConfig) plugin.getConfigService().getConfig(HorseConfig.class);
        if (!horseConfig.isProtectHorseAttackEnabled()) {
            return;
        }

        if (!damageCauses.contains(event.getCause())) {
            return;
        }

        event.setCancelled(true);
    }

}
