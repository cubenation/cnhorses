package de.cubenation.cnhorses.listener;

import de.cubenation.cnhorses.CNHorses;
import de.cubenation.cnhorses.config.HorseConfig;
import de.cubenation.cnhorses.item.HorseSaddle;
import de.cubenation.cnhorses.manager.HorseManager;
import de.cubenation.cnhorses.messages.Messages;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by bhruschka on 28.01.17.
 * Project: CNHorses
 */

@SuppressWarnings("unused")
public class PlayerListener implements Listener {

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onInteractEvent(PlayerInteractEvent event) {
        event.setCancelled(HorseManager.getInstance().handleHorsePlace(event));
    }

    @EventHandler
    public void onDisconnect(PlayerQuitEvent event) {
        HorseConfig horseConfig = (HorseConfig) CNHorses.getInstance().getConfigService().getConfig(HorseConfig.class);
        if (!horseConfig.isDismountOnLogout()) {
            return;
        }

        Player player = event.getPlayer();
        Entity vehicle = player.getVehicle();
        if (vehicle != null) {
            vehicle.removePassenger(player);
        }
    }

    @EventHandler
    public void onPlayerDropItemEvent(PlayerDropItemEvent event) {
        Item itemDrop = event.getItemDrop();
        if (itemDrop == null || itemDrop.getItemStack() == null) {
            return;
        }

        HorseSaddle horseSaddle = new HorseSaddle(itemDrop.getItemStack());
        if (horseSaddle.isHorseSaddle()) {
            Messages.Info.DropSaddle(event.getPlayer(), horseSaddle.getVariant());
        }
    }

}
